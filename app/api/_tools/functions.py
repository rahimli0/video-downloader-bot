
import sys
import os
import re
from uuid import uuid4

import requests as r
import wget
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
from django.conf import settings

from app.settings import TELEGRAM_BOT_TOKEN
import telebot

from bot_app._tools.functions import with_emoji
from bot_app._tools.utils import get_langauge

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)

filedir = os.path.join(settings.MEDIA_ROOT,"videos")
# filedir = MEDIA_ROOT
# filedir = BASE_DIR
ERASE_LINE = '\x1b[2K'

#
# def facebook_video_checked(video_link,language_code):
#     result_list = []
#     for choice in FacebookVideo:
#         try:
#             html = r.get(video_link)
#             sdvideo_url = re.search(choice[0], html.text)[1]
#         except r.ConnectionError as e:
#             print("OOPS!! Connection Error.")
#         except r.Timeout as e:
#             print("OOPS!! Timeout Error")
#         except r.RequestException as e:
#             print("OOPS!! General Error or Invalid URL")
#         except (KeyboardInterrupt, SystemExit):
#             print("Ok ok, quitting")
#             sys.exit(1)
#         except TypeError:
#             print("Video May Private or Invalid URL")
#         else:
#             result_list.append(get_langauge(text=choice[0],lang=language_code))
#     return result_list
#

def youtube_download(choice,video_link,chat_id,language_code):
    from pytube import YouTube
    value_list = [False,'']
    # C:\Python\Python37\python -m venv D:\Ataxan\docker\docker\youtube\env

    yt = YouTube(video_link)
    print("-------------------------------------------------")
    for item in yt.streams.filter(type="video").order_by('resolution'):
        if item.resolution and item.fps:
            if "{} {} {}fps".format(item.resolution, item.mime_type, item.fps) == choice:
                # yt.streams.filter(type="video").order_by('resolution').first().download(output_path=)
                path_folder = '{}'.format(uuid4().hex)
                try:
                    os.mkdir(os.path.join(filedir, path_folder))
                except:
                    pass
                # print(str(item))
                path_data = '{}.{}'.format(uuid4().hex,"{}".format(item.mime_type).replace("video/",""))
                # data_url = s.download(filepath=path_data)
                data = item.download(output_path=os.path.join(filedir,path_folder))
                print("***********************************************************************______________________________")
                print(data)
                file_d = open(data, 'rb')
                print("*************************************______________************************************")
                try:
                    ret_msg = bot.send_video(chat_id, file_d)
                except Exception as e:
                    print(str(e))
                    bot.send_message(chat_id, with_emoji(str(get_langauge(text="Couldn't send the video {}",lang=language_code)), ":label:").format(video_link))
                print("______________________________***********************************************************************")
                # try:
                print(os.path.join(filedir,path_folder))
                os.remove(os.path.join(filedir,path_folder))
                # except:
                #     pass
                value_list = [True]
                break

def vimeo_download(choice,video_link,chat_id,language_code):
    import vimeo_dl as vimeo
    video = vimeo.new(video_link)
    streams = video.streams
    print(streams)
    print("^^^***********************************************______________________________")
    for s in streams:
        print("______________________________________________________")

        print("{} {}".format(s.resolution, s.extension),choice)
        if choice == "{} {}".format(s.resolution, s.extension):
            path_folder = '{}'.format(uuid4().hex)
            try:
                os.mkdir(os.path.join(filedir, path_folder))
            except:
                pass
            path_data = '{}.{}'.format(uuid4().hex,s.extension)
            # data_url = s.download(filepath=path_data)
            data = s.download(filepath=os.path.join(filedir,path_folder, path_data))
            print("***********************************************************************______________________________")
            print(data)
            file_d = open(data, 'rb')
            print("*************************************______________************************************")
            try:
                ret_msg = bot.send_video(chat_id, file_d)
            except:
                bot.send_message(chat_id, with_emoji(str(get_langauge(text="Couldn't send the video {}",lang=language_code)), ":label:").format(video_link))
            print("______________________________***********************************************************************")
            break
        else:
            pass

        print(s)
        print()
    return True
def facebook_download(choice,video_link,chat_id,language_code):
    print("-------------------Facebook Video Downloader-------------------")
    print("1. Download Low Resolution Video")
    print("2. Download High Resolution Video")
    print("3. Exit")
    print(video_link,)
    print(choice)
    print("-------------------*******-------------------------------------")
    if choice == "1":
        try:
            html = r.get(video_link)
            sdvideo_url = re.search('sd_src:"(.+?)"', html.text)[1]
        except r.ConnectionError as e:
            print("OOPS!! Connection Error.")
        except r.Timeout as e:
            print("OOPS!! Timeout Error")
        except r.RequestException as e:
            print("OOPS!! General Error or Invalid URL")
        except (KeyboardInterrupt, SystemExit):
            print("Ok ok, quitting")
            sys.exit(1)
        except TypeError:
            print("Video May Private or Invalid URL")
        else:
            sd_url = sdvideo_url.replace('sd_src:"', '')
            print("\n")
            print("Normal Quality: " + sd_url)
            print("[+] Video Started Downloading")
            path_data = '{}'.format(uuid4().hex)
            try:
                os.mkdir(os.path.join(filedir, path_data))
            except:
                pass
            print("------------------------------------------------------------------------")
            data = wget.download(sd_url, os.path.join(filedir, path_data))
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!______________!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            sys.stdout.write(ERASE_LINE)
            print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^______________^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            file_d = open(data, 'rb')
            print("*************************************______________************************************")
            try:
                ret_msg = bot.send_video(chat_id, file_d)
            except:
                bot.send_message(chat_id, with_emoji(str(get_langauge(text="Couldn't send the video {}",lang=language_code)), ":label:").format(video_link))
            print("\n")
            print(data)
            print("\n")
            print("Video downloaded")
    
    elif choice == "2":
        try:
            html = r.get(video_link)
            hdvideo_url = re.search('hd_src:"(.+?)"', html.text)[1]
        except r.ConnectionError as e:
            print("OOPS!! Connection Error.")
        except r.Timeout as e:
            print("OOPS!! Timeout Error")
        except r.RequestException as e:
            print("OOPS!! General Error or Invalid URL")
        except (KeyboardInterrupt, SystemExit):
            print("Ok ok, quitting")
            sys.exit(1)
        except TypeError:
            print("Video May Private or Hd version not avilable")
        else:
            hd_url = hdvideo_url.replace('hd_src:"', '')
            print("\n")
            print("High Quality: " + hd_url)
            print("[+] Video Started Downloading")
            path_data = '{}'.format(uuid4().hex)
            try:
                os.mkdir(os.path.join(filedir, path_data))
            except:
                pass
            data = wget.download(hd_url, os.path.join(filedir, path_data))
            sys.stdout.write(ERASE_LINE)
            file_d = open(data, 'rb')
            try:
                ret_msg = bot.send_video(chat_id, file_d)
            except:
                bot.send_message(chat_id, with_emoji(str(get_langauge(text="Couldn't send the video {}",lang=language_code)), ":label:").format(video_link))
            print("\n")
            print(data)
            print("\n")
            print("Video downloaded")
    
    elif choice == "3":
        print("Exiting")
    
    else:
        print("[-] Invalid option!")

# facebook_download("1","https://www.facebook.com/SkySports/videos/10153310275743762/?type=2&theater")