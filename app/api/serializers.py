from rest_framework import serializers
from django.contrib.auth import get_user_model, authenticate

from bot_app._tools.utils import get_langauge


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('username','password','first_name','last_name',)
        extra_kwargs = {'password':{'write_only':True, 'min_length':5}}
    def create(self,validated_data):
        return get_user_model().objects.create(**validated_data)
    def update(self, instance, validated_data):
        password = validated_data.pop('password',None)
        user = super().update(instance,validated_data)
        if password:
            user.set_password(password)
            user.save()
        return user


class AuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField()
    language = serializers.CharField()
    password = serializers.CharField(
        style={'input_type':'password'},
        trim_whitespace=False
    )
    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')
        language = attrs.get('language')
        user = authenticate(
            request=self.context.get('request'),
            username=username,
            password=password
        )
        if not user:
            msg = get_langauge(text="Unable to authenticate with provided credentials",lang=language)
            raise serializers.ValidationError(msg, code='authentication')
        attrs['user'] = user
        return attrs