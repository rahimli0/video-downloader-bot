from django.urls import path

from api import views
from bot_app import views as bot_app_views

app_name = 'core-user'

urlpatterns = [
	path('update/', bot_app_views.UpdateBot.as_view(), name='update'),
    path('get-started/', views.CreateOrGetAPIView.as_view(), name='get-started'),
    path('create/', views.CreateUserView.as_view(), name='create'),
    path('user/', views.CreateTokenView.as_view(), name='user'),
	path('next-step-status/', views.NextStepStatus.as_view(), name='next-step-status'),
	path('video-download-view/', views.VideoDownloadView.as_view(), name='video-download-view'),
    # path('me/', views.ManageUserViewView.as_view(), name='me'),
    path('me/', views.ManageUserApiView.as_view(), name='me'),
]