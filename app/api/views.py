from django.contrib.auth import get_user_model
from rest_framework import generics, renderers, authentication, permissions
from rest_framework import views
from rest_framework.response import Response
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings

from api._tools.functions import facebook_download, vimeo_download, youtube_download
from app.settings import TELEGRAM_BOT_TOKEN
from api.serializers import UserSerializer, AuthTokenSerializer
# Create your views here.
from bot_app._tools.utils import get_langauge, MenuQualityDATAKeyList
from core_user.models import AllUserStatus

GUser = get_user_model()
import telebot
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, CallbackQuery, InlineKeyboardMarkup, InlineKeyboardButton
bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)


class CreateUserView(generics.CreateAPIView):
    serializer_class = UserSerializer

class CreateOrGetAPIView(views.APIView):
    # authentication_classes = (authentication.TokenAuthentication,)
    # permission_classes = (permissions.IsAuthenticated, )
    # serializer_class = UserSerializer
    renderer_classes = (renderers.JSONRenderer,)
    def post(self,request):
        language = request.POST.get("language")
        chat_id = request.POST.get("chat_id")
        # try:
        try:
            user = AllUserStatus.objects.get(chat_id=chat_id)
            code = 1
        except:
            user = AllUserStatus.objects.create(chat_id=chat_id,language=language)
            code = 1
        next_step_status = user.next_step_status
        text = get_langauge(text="Welcomeback",lang=user.language)
        # except:
        #     code = 0
        #     text = ''
        return Response(
            data={
                'text':text,
                'code':code,
                'next_step_status':next_step_status,
            }
        )



class NextStepStatus(views.APIView):
    def post(self,request):
        ns_status = request.POST.get('ns_status', '-')
        url_data = request.POST.get('url_data', None)
        language = request.POST.get("language")
        chat_id = request.POST.get("chat_id")
        code = 0
        if ns_status:
            try:
                user = AllUserStatus.objects.get(chat_id=chat_id)
                code = 1
            except:
                user = AllUserStatus.objects.create(chat_id=chat_id, language=language)
                code = 1
            user.next_step_status = ns_status
            if ns_status == "-":
                user.url_data = None
            if url_data:
                user.url_data = url_data
            user.save()
            data = {'code':code}
        else:
            data = {'code':code}
        return Response(data)



class VideoDownloadView(views.APIView):
    def post(self,request):
        chat_id = request.POST.get("chat_id")
        language = request.POST.get("language")
        choice_quality = request.POST.get("choice_quality")
        code = 0
        text = ''
        try:
            user = AllUserStatus.objects.get(chat_id=chat_id)
            # code = 1
        except:
            user = AllUserStatus.objects.create(chat_id=chat_id, language=language)
            # code = 1
        print("{} *-* {}".format(user.next_step_status, user.next_step_status))
        if user.next_step_status in MenuQualityDATAKeyList:
            if user.next_step_status == MenuQualityDATAKeyList[0]:
                if choice_quality and user.url_data:
                    import _thread
                    try:
                        _thread.start_new_thread(youtube_download,(choice_quality, user.url_data,chat_id,language))
                        code = 1
                        text = get_langauge(text="Video is going to send in a few minute",lang=language)
                    except:
                        text = get_langauge(text="Something wrong happens",lang=language)
                    user.next_step_status = "-"
                    user.save()
                else:
                    text = get_langauge(text="Choose correct quality",lang=language)
            elif user.next_step_status == MenuQualityDATAKeyList[1]:
                pass
            elif user.next_step_status == MenuQualityDATAKeyList[2]:
                if choice_quality and user.url_data:
                    import _thread
                    try:
                        _thread.start_new_thread(facebook_download,(choice_quality, user.url_data,chat_id,language))
                        code = 1
                        text = get_langauge(text="Video is going to send in a few minute",lang=language)
                    except:
                        text = get_langauge(text="Something wrong happens",lang=language)
                    user.next_step_status = "-"
                    user.save()
                else:
                    text = get_langauge(text="Choose correct quality",lang=language)
            elif user.next_step_status == MenuQualityDATAKeyList[3]:
                if choice_quality and user.url_data:
                    # vimeo_download(choice_quality, user.url_data, chat_id)
                    import _thread
                    try:
                        _thread.start_new_thread(vimeo_download,(choice_quality, user.url_data,chat_id,language))
                        code = 1
                        text = get_langauge(text="Video is going to send in a few minute",lang=language)
                    except:
                        text = get_langauge(text="Something wrong happens",lang=language)
                    user.next_step_status = "-"
                    user.save()
                else:
                    text = get_langauge(text="Choose correct quality",lang=language)
            else:
                code = 0
        else:
            code = 0
        user.save()
        data = {'code':code,'text':text}
        return Response(data)



class CreateTokenView(ObtainAuthToken):

    serializer_class = AuthTokenSerializer
    renderer_classes = (renderers.JSONRenderer,)


class ManageUserViewView(generics.RetrieveUpdateAPIView):

    serializer_class = UserSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated)
    renderer_classes = (renderers.JSONRenderer,)
    def get_object(self):
        return self.request.user

class ManageUserApiView(generics.RetrieveAPIView):

    serializer_class = UserSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated)
    renderer_classes = (renderers.JSONRenderer,)
    def get_object(self):
        return self.request.user