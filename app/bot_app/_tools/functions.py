import _thread

import emoji
import requests
from rest_framework.response import Response
from rest_framework.views import APIView
import json

from bot_app._tools.utils import RequestHeaders
from bot_app.general.utils import MainMenuList, ParentMenuList, MainBackText, BackRegisterHandler
# from bot_app.general.widgets import MenuButton
from app.settings import TELEGRAM_BOT_TOKEN

from telebot import types
import telebot

domain_name = 'http://127.0.0.1:8000'
api_base_url = 'http://127.0.0.1:8000/api/'

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)


def with_emoji(text, emoji_pattern):
    return emoji.emojize('{} {}'.format(emoji_pattern, text))

def auth_check(message):
    return_val = [False, '',0,"-",None]
    # print(message)
    # try:
    url = '{}get-started/'.format(api_base_url)
    r = requests.post(url,
      data={
          'chat_id': message.from_user.id,
          'language': message.from_user.language_code,
      },
      # headers=RequestHeaders
    )
    # r.headers = RequestHeaders
    data = json.loads(r.text)
    print(r.status_code)
    print(url)
    return_val = [True if data['code'] else False , data['text'], '',data['next_step_status'],data]
    # except:
    #     text = "@{}, please try again later".format(message.from_user.username)
    #     bot.send_message(message.chat.id, text)

    return return_val

def check_main_menu(message):
    # bot.send_message(message.chat.id, "---------------- check_main_menu -----------------".format(2))
    return_val = False
    if message.text in MainMenuList or message.text in ParentMenuList:
        return_val = True
    return return_val