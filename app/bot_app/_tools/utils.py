import emoji

domain_name = 'https://e63dd0d9.ngrok.io'
image_domain_name = 'https://e63dd0d9.ngrok.io'

RequestHeaders={'Authorization': 'Token ddbb29804209fec5c9a56e0cacd7f1b10f521a66','Content-Type':'application/json','Accept':'application/json'}

MainBackText = emoji.emojize(':BACK_arrow: Cancel')
BackMainMenuText = emoji.emojize(':BACK_arrow: Back to menu')
MenuDATA = (
    (emoji.emojize(':heavy_plus_sign:  Youtube'),'youtube_url'),
    (emoji.emojize(':heavy_plus_sign:  Instagram'),'instagram_url'),
    (emoji.emojize(':heavy_plus_sign:  Facebook'),'facebook_url'),
    (emoji.emojize(':heavy_plus_sign:  Vimeo'),'vimeo_url'),
)
MenuQualityDATA = (
    (emoji.emojize(':heavy_plus_sign:  Youtube'),'youtube_url_quality_menu'),
    (emoji.emojize(':heavy_plus_sign:  Instagram'),'instagram_url_quality_menu'),
    (emoji.emojize(':heavy_plus_sign:  Facebook'),'facebook_url_quality_menu'),
    (emoji.emojize(':heavy_plus_sign:  Vimeo'),'vimeo_url_quality_menu'),
)
MenuDATAList = [item[0] for item in MenuDATA]
MenuDATAKeyList = [item[1] for item in MenuDATA]
MenuQualityDATAKeyList = [item[1] for item in MenuQualityDATA]

FacebookVideo = (
    ('sd_src:"(.+?)"', "Download Low Resolution Video"),
    ('hd_src:"(.+?)"', "Download High Resolution Video"),
)


LANGAUGESMENUKEYS = [":BACK_arrow: Cancel",":BACK_arrow: Back to menu","Download Low Resolution Video","Download High Resolution Video",]

# FacebookVideo

LANGAUGESMENU = {
    "en":{
        ":BACK_arrow: Cancel":LANGAUGESMENUKEYS[0],
        ":BACK_arrow: Back to menu":LANGAUGESMENUKEYS[1],
        "download Low Resolution Video":LANGAUGESMENUKEYS[2],
        "download High Resolution Video":LANGAUGESMENUKEYS[3],
    },
    "tr":{
        ":BACK_arrow: Vaz geç":LANGAUGESMENUKEYS[0],
        ":BACK_arrow: Menyüye dön":LANGAUGESMENUKEYS[1],
        "videoyu düşük çözünürlükte indirin":LANGAUGESMENUKEYS[2],
        "videoyu yüksek çözünürlükte indirin":LANGAUGESMENUKEYS[3],
    },
    "ru":{
        ":BACK_arrow: Cancel":LANGAUGESMENUKEYS[0],
        ":BACK_arrow: Back to menu":LANGAUGESMENUKEYS[1],
        "Скачать видео в низком разрешении":LANGAUGESMENUKEYS[2],
        "скачать видео в высоком разрешении":LANGAUGESMENUKEYS[3],
    },
}

LANGAUGES = {
    "Menu":{
        "en": "Menu",
        "ru": "Мену",
        "tr": "Menü",
        "ar": "أهلا بك",
        "in": "स्वागत हे",
    },
    "Welcomeback":{
        "en": "Welcomeback",
        "ru": "Добро пожаловать",
        "tr": "Hoşgeldiniz",
        "ar": "أهلا بك",
        "in": "स्वागत हे",
    },
    "Couldn't send the video {}":{
        "en": "Couldn't send the video {}",
        "ru": "Не удалось отправить видео {}",
        "tr": "Video gönderilemedi {}",
        "ar": " {} تعذر إرسال الفيديو",
        "in": "वीडियो नहीं भेजा जा सका {}",
    },
    "Your request accepted and will processed as soon as":{
        "en": "Your request accepted and will processed as soon as",
        "ru": "Ваш запрос принят. подождите",
        "tr": "İsteğiniz kabul edildi ve en kısa sürede işleme koyulacak",
        "ar": "تم قبول طلبك. أرجو الإنتظار",
        "in": "आपका अनुरोध स्वीकार कर लिया गया। कृपया प्रतीक्षा करें",
    },
    "Video is going to send in a few minute":{
        "en": "Video is going to send in a few minute",
        "ru": "видео отправят через несколько минут",
        "tr": "video birkaç dakika içinde gönderilecek",
        "ar": "سيتم إرسال الفيديو خلال بضع دقائق",
        "in": "वीडियो कुछ ही मिनटों में भेजने वाला है",
    },
    "Choose video quality":{
        "en": "Choose video quality",
        "ru": "Выберите качество видео",
        "tr": "Video kalitesini seçin",
        "ar": "اختر جودة الفيديو",
        "in": "वीडियो की गुणवत्ता चुनें",
    },
    "Video may private or invalid url":{
        "en": "Video may private or invalid url",
        "ru": "Видео может быть частным или недействительным УРЛ",
        "tr": "Video özel veya geçersiz URL olabilir",
        "ar": "قد يكون الفيديو خاص أو غير صالح",
        "in": "वीडियो निजी या अमान्य url हो सकता है",
    },
    "Unable to authenticate with provided credentials":{
        "en": "Unable to authenticate with provided credentials",
        "ru": "Невозможно пройти аутентификацию с предоставленными учетными данными",
        "tr": "Sağlanan kimlik bilgileriyle kimlik doğrulaması yapılamıyor",
        "ar": "تعذرت المصادقة باستخدام بيانات الاعتماد المقدمة",
        "in": "प्रदान की गई प्रमाणिकता के साथ प्रमाणित करने में असमर्थ",
    },
    "Download Low Resolution Video":{
        "en":"Download Low Resolution Video",
        "tr":"videoyu düşük çözünürlükte indirin",
        "ru":"Скачать видео в низком разрешении",
    },
    "Download High Resolution Video":{
        "en":"Download High Resolution Video",
        "tr":"videoyu yüksek çözünürlükte indirin",
        "ru":"Скачать видео высокого разрешения",
    },
    "Try again":{
        "en":"Try again",
        "tr":"Try again",
        "ru":"Try again",
    },
    "type your {} link":{
        "en":"type your {} link",
        "tr":"{} bağlantınızı girin",
        "ru":"введите ссылку {}",
    },
    "type a {} link or profile name":{
        "en":"type your {} link",
        "tr":"bir {} bağlantı veya profil adı girin",
        "ru":"введите {} ссылку или имя профиля",
    },
    "please type correct {} link":{
        "en":"type your {} link",
        "tr":"lütfen doğru {} bağlantısı girin",
        "ru":"пожалуйста, введите правильную ссылку {}",
    },
    "please type correct {} link or profile name":{
        "en":"please type correct {} link or profile name",
        "tr":"lütfen doğru {} bağlantısı veya profil adı girin",
        "ru":"пожалуйста, введите правильную {} ссылку или имя профиля",
    },
    ":BACK_arrow: Cancel":{
        "en":":BACK_arrow: Cancel",
        "tr":":BACK_arrow: Cancel",
        "ru":":BACK_arrow: Cancel",
    },
}
LanguageCHOICES = (
    ('en', 'English'),
    ('ru', 'Russian'),
    ('tr', 'Turkish'),
    ('ar', 'Aabic'),
    ('in', 'Hindi'),
)
def get_langauge(text,lang):
    try:
        text=LANGAUGES["{}".format(text)]
        try:
            text = text["{}".format(lang)]
        except:
            text = text["{}".format(LanguageCHOICES[0][0])]
    except:
        pass
    return text

def get_menu_langauge(text,lang):
    # print("---------------")
    # print(text)
    # print(lang)
    # print("---------------")
    try:
        try:
            text_data = LANGAUGESMENU["{}".format(lang)]
        except:
            text_data = LANGAUGESMENU["{}".format(LanguageCHOICES[0][0])]
        text=text_data["{}".format(text)]
    except:
        text = ''
    return text

def get_menu_decode(lang,index):
    try:
        try:
            text_data = LANGAUGESMENU["{}".format(lang)]
        except:
            text_data = LANGAUGESMENU["{}".format(LanguageCHOICES[0][0])]
        print(text_data)
        text=list(dict(text_data).keys())[index]
    except:
        text = ''
    return text