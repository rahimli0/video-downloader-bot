import json
import sys

import emoji
import requests
from app.settings import TELEGRAM_BOT_TOKEN

from telebot import types
import telebot

from bot_app._tools.functions import api_base_url

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
from bot_app._tools.utils import domain_name, MenuDATA, MenuDATAKeyList, MainBackText, BackMainMenuText, \
    get_menu_langauge, LANGAUGESMENUKEYS, LANGAUGESMENU, get_menu_decode, FacebookVideo, get_langauge

import re
import requests as r


class VideoDownload:
    def __init__(self, chat_id):
        self.chat_id = chat_id
    def video_download(self,s_type,language_code,choice_quality=None):
        code = 0
        text = ""
        data={
            'chat_id': self.chat_id,
            'language': language_code,
            'choice_quality':choice_quality,
        }
        r = requests.post('{}video-download-view/'.format(api_base_url,),
            data=data
        )
        data = json.loads(r.text)
        code = data['code']
        text = data['text']
        return [code,text]

class CustomValidate:
    def youtube_url_validation(self, url):
        youtube_regex = (
            r'(https?://)?(www\.)?'
            '(youtube|youtu)\.(com|be)/'
            '(watch\?v=|embed/|v/|.+\?v=)?([^&=%\?]{11})')

        youtube_regex_match = re.match(youtube_regex, url)
        if youtube_regex_match:
            return youtube_regex_match

    def youtube_video_quality(self, url):
        from pytube import YouTube
        value_list = []
        # C:\Python\Python37\python -m venv D:\Ataxan\docker\docker\youtube\env

        yt = YouTube(url)
        print("-------------------------------------------------")
        for item in yt.streams.filter(type="video"):
            if item.resolution and item.fps:
                value_list.append("{} {} {}fps".format(item.resolution,item.mime_type,item.fps))
            print(item)

        return value_list


    def vimeo_video_quality(self, url):
        import vimeo_dl as vimeo
        value_list = []
        try:
            video_vimeo = vimeo.new(url)
            streams_vimeo = video_vimeo.streams
            for stream_item in streams_vimeo:
                if stream_item.resolution and stream_item.extension:
                    value_list.append("{} {}".format(stream_item.resolution,stream_item.extension))
            resut = True
        except:
            resut = False
        return [resut, value_list]

    def facebook_video_checked(self,video_link,language_code):
        result_list = []
        for choice in FacebookVideo:
            try:
                html = r.get(video_link)
                sdvideo_url = re.search(choice[0], html.text)[1]
            except r.ConnectionError as e:
                print("OOPS!! Connection Error.")
            except r.Timeout as e:
                print("OOPS!! Timeout Error")
            except r.RequestException as e:
                print("OOPS!! General Error or Invalid URL")
            except (KeyboardInterrupt, SystemExit):
                print("Ok ok, quitting")
                sys.exit(1)
            except TypeError:
                print("Video May Private or Invalid URL")
            else:
                result_list.append(get_langauge(text=choice[1],lang=language_code))
        return result_list


class MenuButtonModel:
    def __init__(self, message, text):
        self.message = message
        self.text = text
        self.parent = None
    def with_emoji(self,text,emoji_pattern):
        return emoji.emojize('{} {}'.format(emoji_pattern,text))

    def general_menu(self):
        code = 0
        text = "Yenidən cəhd edin2"
        usertype = 0
        # try:
        url = '{}get-started/'.format(api_base_url)
        r = requests.post(url,
          data={
              'chat_id': self.message.from_user.id,
              'language': self.message.from_user.language_code,
          },
          # headers=RequestHeaders
        )
        data = json.loads(r.text)
        text = data['text']
        language_code = self.message.from_user.language_code
        if data['code']:
            key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
            if data['next_step_status'] and data['next_step_status'] != "-":
                if data['next_step_status'] in MenuDATAKeyList:
                    key.row_width = 1
                    key.add(
                        types.KeyboardButton(text=emoji.emojize(get_menu_decode(language_code,0))),
                    )
                else:
                    key.row_width = 2
                    key.add(
                        types.KeyboardButton(text=MenuDATA[0]),
                        types.KeyboardButton(text=MenuDATA[1]),
                    )
            else:
                key.row_width = 2
                key.add(
                    types.KeyboardButton(text=MenuDATA[0][0]),
                    types.KeyboardButton(text=MenuDATA[1][0]),
                    types.KeyboardButton(text=MenuDATA[2][0]),
                    types.KeyboardButton(text=MenuDATA[3][0]),
                )
            if self.text:
                bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(self.text,":label:"), reply_markup=key, parse_mode='HTML')
            else:
                bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(self.text,":label:"), reply_markup=key, parse_mode='HTML')
        else:
            bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(text,":label:"),parse_mode='HTML')
        # except:
        #     bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(text,":label:"),parse_mode='HTML')

    def general_menu_data(self,data):
        code = 0
        text = "Yenidən cəhd edin2"
        # try:
        text = data['text']
        language_code = self.message.from_user.language_code
        if data['code']:
            key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
            if data['next_step_status'] and data['next_step_status'] != "-":
                if data['next_step_status'] in MenuDATAKeyList:
                    key.row_width = 1
                    key.add(
                        types.KeyboardButton(text=emoji.emojize(get_menu_decode(language_code,0))),
                    )
                else:
                    key.row_width = 2
                    key.add(
                        types.KeyboardButton(text=MenuDATA[0]),
                        types.KeyboardButton(text=MenuDATA[1]),
                    )
            else:
                key.row_width = 2
                key.add(
                    types.KeyboardButton(text=MenuDATA[0][0]),
                    types.KeyboardButton(text=MenuDATA[1][0]),
                    types.KeyboardButton(text=MenuDATA[2][0]),
                    types.KeyboardButton(text=MenuDATA[3][0]),
                )
            if self.text:
                bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(self.text,":label:"), reply_markup=key, parse_mode='HTML')
            else:
                bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(self.text,":label:"), reply_markup=key, parse_mode='HTML')
        else:
            bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(text,":label:"),parse_mode='HTML')
        # except:
        #     bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(text,":label:"),parse_mode='HTML')


    def custom_menu_send(self,key):
        try:
            bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(self.text, ":label:"),
                             reply_markup=key, parse_mode='HTML')
        except:
            bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(self.text,":label:"),parse_mode='HTML')
        # except:
        #     bot.send_message(chat_id=self.message.from_user.id, text=self.with_emoji(text,":label:"),parse_mode='HTML')


class RegisterStepModel:
    def __init__(self,chat_id):
        self.chat_id = chat_id
    def set_status(self,ns_status,extra_value,language_code):
        code = 0
        try:
            data={
                'ns_status': ns_status,
                'chat_id': self.chat_id,
                'language': language_code,
            }
            if extra_value:
                data['{}'.format(extra_value[0])] = extra_value[1]
            r = requests.post('{}next-step-status/'.format(api_base_url,),
                data=data
            )
            data = json.loads(r.text)
            if data['code']:
                code = 1
        except:
            pass
        return code
