import os

from celery import Celery, shared_task

from core_user.tools.common import USERGAMESTATUS
from bot_app.general.utils import TOTALUSERGAMEPENALTYPOINT
from bot_app.tools.tools import RepeatedTimer
from bot_app.general.widgets import GameEditModel, GameModel

from app.settings import TELEGRAM_CHANNEL_NAME
# from threading import Timer
from time import sleep
def xrange(x):

    return iter(range(x))

@shared_task
def one_delete_messages(c_id,l_m_id):
    import requests
    from app.settings import TELEGRAM_BOT_TOKEN
    try:
        r2 = requests.get('https://api.telegram.org/bot{}/deleteMessage?chat_id={}&message_id={}'.format(
            TELEGRAM_BOT_TOKEN, c_id, l_m_id))
    except:
        pass

@shared_task
def channel_message(text):
    import requests
    from app.settings import TELEGRAM_BOT_TOKEN
    try:
        import telebot
        bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)
        bot.send_message(chat_id=TELEGRAM_CHANNEL_NAME,text=text)
        # r2 = requests.post('https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}'.format(
        #     TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_NAME, text))
    except:
        pass

def none_to_space(data):
    if data is None:
        return ''
    else:
        return data

def len_array(arr):
    new_arr = []
    for x in arr:
        if str(x).strip():
            new_arr.append(x)
    return_val = len(new_arr)
    return return_val

def split_data(deff):
  new_arr = []
  for x in deff.split('&'):
      if str(x).strip():
          new_arr.append(str(x).strip())
  return new_arr
