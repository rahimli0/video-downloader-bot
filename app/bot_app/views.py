import copy
import datetime

import emoji
from django.contrib.auth import get_user_model
from rest_framework.response import Response
from rest_framework.views import APIView
from telebot import types

from api._tools.functions import facebook_download
from app.settings import TELEGRAM_BOT_TOKEN
import telebot
# # from bot_app.general.functions import *
# from bot_app.general.handler_functions import *
import _thread


from telebot.types import ReplyKeyboardMarkup, KeyboardButton, CallbackQuery, InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardRemove

from bot_app._tools.functions import auth_check, check_main_menu
from bot_app._tools.utils import MenuDATAList, MenuDATA, get_langauge, get_menu_langauge, LANGAUGESMENUKEYS, \
    MenuDATAKeyList, get_menu_decode, MenuQualityDATAKeyList
from bot_app._tools.widgets import MenuButtonModel, RegisterStepModel, CustomValidate, VideoDownload
from bot_app.general.functions import check_step_handlerstate

GUser = get_user_model()
# https://api.telegram.org/bot1000027230:AAErodfcEWWBsbTndnChewJtrrtrtrtrPidhVszIhnk3jSnmM/setWebhook?url=https://www.book-plays.com//api/update/

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)

# menu - Operation menu
# me - About myself
# setpoint - Set point to user
# show_section_user_info - Show user info
# show_user_info - Show section user info
# user_info - Show user info


cm = CustomValidate()

class UpdateBot(APIView):
    def post(self,request):
        json_string = request.body.decode("UTF-8")
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        # bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
        return Response({'code':200})


def extract_unique_code(text):
    # Extracts the unique_code from the sent /start command.
    return text.split()[1] if len(text.split()) > 1 else None


@bot.message_handler(commands=['start'])
def start(message):
    general_message = copy.deepcopy(message)
    auth_check_data = auth_check(general_message)

    unique_code = extract_unique_code(general_message.text)
    if auth_check_data[0]:
        main_data = auth_check_data[4]
        mbModel = MenuButtonModel(message=general_message,text=auth_check_data[2])
        mbModel.general_menu_data(main_data)
        # bot.send_message(general_message.chat.id,text=auth_check_data[2])
        if unique_code:
            pass
        else:
            pass

#
@bot.message_handler(func=lambda message: True, content_types=['text','contact'])
def command_default(message):
    # if check_main_menu(message=message):
    #     menu_handler(message=message)
    # else:
    general_message = copy.deepcopy(message)
    auth_check_data = auth_check(general_message)
    language_code = general_message.from_user.language_code
    if auth_check_data[0]:
        main_data = auth_check_data[4]
        step_handler_state = auth_check_data[3]
        # print(get_menu_langauge(general_message.text, language_code))
        # print(emoji.demojize(general_message.text))
        # print(LANGAUGESMENUKEYS[0])
        if get_menu_langauge(emoji.demojize(general_message.text), language_code) == LANGAUGESMENUKEYS[0]:
            # print("************************************")
            rm = RegisterStepModel(chat_id=general_message.chat.id)
            rm.set_status(ns_status="-", extra_value=None, language_code=language_code)
            mb = MenuButtonModel(message=general_message,text=get_langauge(text="Menu",lang=language_code))
            mb.general_menu()
            # mb.general_menu_data(main_data)
        elif step_handler_state == "-":
            if general_message.text in MenuDATAList:
                next_status = "-"
                text = get_langauge("Try again",language_code)
                if general_message.text == MenuDATA[0][0]:
                    text = get_langauge("type your {} link",language_code).format('youtube')
                    next_status = MenuDATA[0][1]
                elif general_message.text == MenuDATA[1][0]:
                    text = get_langauge("type a {} link or profile name",language_code).format('instagram')
                    next_status = MenuDATA[1][1]
                elif general_message.text == MenuDATA[2][0]:
                    text = get_langauge("type your {} link",language_code).format('facebook')
                    next_status = MenuDATA[2][1]
                elif general_message.text == MenuDATA[3][0]:
                    text = get_langauge("type your {} link",language_code).format('vimeo')
                    next_status = MenuDATA[3][1]
                rm  = RegisterStepModel(chat_id=general_message.chat.id)
                rm.set_status(ns_status=next_status,extra_value=None,language_code=language_code)
                mb = MenuButtonModel(message=general_message,text=text)
                mb.general_menu()
                # mb.general_menu_data(main_data)
                # bot.send_message(chat_id=general_message.chat.id,text=)
            else:
                mbModel = MenuButtonModel(message=general_message,text=auth_check_data[2])
                # mbModel.general_menu()
                mbModel.general_menu_data(main_data)

        else:
            if step_handler_state in MenuDATAKeyList:
                if step_handler_state == MenuDATA[0][1]:
                    if cm.youtube_url_validation(general_message.text):
                        key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
                        data_list = cm.youtube_video_quality(url=general_message.text)
                        key.row_width = 1
                        key.add(
                            types.KeyboardButton(text=emoji.emojize(get_menu_decode(language_code,0))),
                        )
                        # print(data_list)
                        # print(data_list)
                        if len(data_list):
                            for data_list_item in data_list:
                                key.add(
                                    types.KeyboardButton(text=get_langauge(text=data_list_item, lang=language_code)),
                                )
                        text = get_langauge("Choose video quality", language_code).format('youtube')

                        rm = RegisterStepModel(chat_id=general_message.chat.id)
                        rm.set_status(ns_status="youtube_url_quality_menu", extra_value=('url_data',general_message.text), language_code=language_code)

                        mbModel = MenuButtonModel(message=general_message, text=text)
                        mbModel.custom_menu_send(key=key)
                    else:
                        text = get_langauge("please type correct {} link", language_code).format('youtube')
                        next_status = MenuDATA[0][1]
                        mbModel = MenuButtonModel(message=general_message, text=text)
                        # mbModel.general_menu()
                        mbModel.general_menu_data(main_data)
                elif step_handler_state == MenuDATA[1][1]:
                    # text = get_langauge("please type correct {} link or profile name", language_code).format('instagram')
                    next_status = MenuDATA[1][1]
                elif step_handler_state == MenuDATA[2][1]:
                    data_list = cm.facebook_video_checked(video_link=general_message.text,language_code=language_code)
                    key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)

                    if len(data_list):
                        key.row_width = 1
                        key.add(
                            types.KeyboardButton(text=emoji.emojize(get_menu_decode(language_code,0))),
                        )
                        for data_list_item in data_list:
                            key.add(
                                types.KeyboardButton(text=get_langauge(text=data_list_item, lang=language_code)),
                            )
                        text = get_langauge("Choose video quality", language_code).format('facebook')

                        rm = RegisterStepModel(chat_id=general_message.chat.id)
                        rm.set_status(ns_status="facebook_url_quality_menu", extra_value=('url_data',general_message.text), language_code=language_code)


                        mbModel = MenuButtonModel(message=general_message, text=text)
                        mbModel.custom_menu_send(key=key)
                    else:
                        text = get_langauge("Video may private or invalid url", language_code)
                        text = "{}\n{}".format(text,get_langauge("please type correct {} link", language_code).format('facebook'))
                        mbModel = MenuButtonModel(message=general_message, text=text)
                        # mbModel.general_menu()
                        mbModel.general_menu_data(main_data)
                elif step_handler_state == MenuDATA[3][1]:
                    data_list_data = cm.vimeo_video_quality(url=general_message.text)
                    if data_list_data[0] and len(data_list_data[1])>0:
                        key = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
                        key.row_width = 1
                        key.add(
                            types.KeyboardButton(text=emoji.emojize(get_menu_decode(language_code,0))),
                        )
                        data_list = data_list_data[1]
                        if len(data_list):
                            for data_list_item in data_list:
                                key.add(
                                    types.KeyboardButton(text=get_langauge(text=data_list_item, lang=language_code)),
                                )
                        text = get_langauge("Choose video quality", language_code).format('vimeo')

                        rm = RegisterStepModel(chat_id=general_message.chat.id)
                        rm.set_status(ns_status="vimeo_url_quality_menu", extra_value=('url_data',general_message.text), language_code=language_code)

                        mbModel = MenuButtonModel(message=general_message, text=text)
                        mbModel.custom_menu_send(key=key)
                    else:
                        text = get_langauge("please type correct {} link", language_code).format('vimeo')
                        mbModel = MenuButtonModel(message=general_message, text=text)
                        mbModel.general_menu_data(main_data)
            elif step_handler_state in MenuQualityDATAKeyList :
                videoDownloadModel = VideoDownload(chat_id=general_message.chat.id)
                if step_handler_state == MenuQualityDATAKeyList[0]:
                    # hideBoard = types.ReplyKeyboardRemove()
                    # bot.send_message(chat_id=general_message.chat.id,text=get_langauge(text="Your request accepted. Please wait",lang=language_code,))
                    # if general_message.text in data_list:
                    # bot.send_message(chat_id=general_message.chat.id,text="oh yeah")
                    resultData = videoDownloadModel.video_download(s_type="youtube",language_code=language_code,choice_quality=general_message.text)
                    rm = RegisterStepModel(chat_id=general_message.chat.id)
                    rm.set_status(ns_status="-", extra_value=None, language_code=language_code)
                    mbModel = MenuButtonModel(message=general_message, text=get_langauge(text="Your request accepted and will processed as soon as",lang=language_code,))
                    mbModel.general_menu()

                    # else:
                    #     bot.send_message(chat_id=general_message.chat.id,text="try again")
                        # mbModel = MenuButtonModel(message=general_message, text=text)
                        # mbModel.custom_menu_send(key=key)
                elif step_handler_state == MenuQualityDATAKeyList[1]:
                    pass
                elif step_handler_state == MenuQualityDATAKeyList[2]:
                    if get_menu_langauge(emoji.demojize(general_message.text), language_code) == LANGAUGESMENUKEYS[2]:
                        resultData = videoDownloadModel.video_download(s_type="facebook",language_code=language_code,choice_quality="1")
                        menuButtonModel = MenuButtonModel(message=general_message,text=resultData[1])
                        menuButtonModel.general_menu()
                    elif get_menu_langauge(emoji.demojize(general_message.text), language_code) == LANGAUGESMENUKEYS[3]:
                        resultData = videoDownloadModel.video_download(s_type="facebook",language_code=language_code,choice_quality="2")
                        menuButtonModel = MenuButtonModel(message=general_message,text=resultData[1])
                        menuButtonModel.general_menu()
                    else:
                        bot.send_message(chat_id=general_message.chat.id,text="try again")
                elif step_handler_state == MenuQualityDATAKeyList[3]:
                    try:
                        resultData = videoDownloadModel.video_download(s_type="vimeo",language_code=language_code,choice_quality=general_message.text)
                        rm = RegisterStepModel(chat_id=general_message.chat.id)
                        rm.set_status(ns_status="-", extra_value=None, language_code=language_code)
                        mbModel = MenuButtonModel(message=general_message, text=get_langauge(text="Your request accepted and will processed as soon as",lang=language_code,))
                        mbModel.general_menu()
                    except:
                        bot.send_message(chat_id=general_message.chat.id,text="try again")
            else:
                mbModel = MenuButtonModel(message=general_message, text=auth_check_data[2])
                # mbModel.general_menu()
                mbModel.general_menu_data(main_data)

    #             register_contact_handler(message=general_message)
    #         elif step_handler_state == AllHandlersList[1]:
    #             register_character_name_handler(message=general_message)
    #         elif step_handler_state == AllHandlersList[2]:
    #             register_gender_handler(message=general_message)
    #         elif step_handler_state == AllHandlersList[3]:
    #             register_avatar_handler(message=general_message)
    #         elif step_handler_state == AllHandlersList[4]:
    #             register_strategy_handler(message=general_message)
    #         elif step_handler_state == AllHandlersList[5]:
    #             private_game_handler(message=general_message)
    #     else:
    #         auth_check_data = auth_check(general_message)
    #         if auth_check_data[0]:
    #             if auth_check_data[2] == USERGAMESTATUS[0][0]:
    #
    #                 gm = GameModel(chat_id=general_message.chat.id)
    #                 gm.send_main_fight_menu()
    #                 import _thread
    #                 try:
    #                     _thread.start_new_thread(delete_message_one_message,(message.chat.id, message.message_id))
    #                 except:
    #                     pass
    #             else:
    #                 pass
    #
    #
    #



