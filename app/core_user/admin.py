from django.contrib import admin
from core_user.models import MyUser, AllUserStatus
from core_user.forms import MyUserChangeForm, MyUserCreationForm
# Register your models here.
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model

User = get_user_model()

# Admin panel actions here
def make_verified_user(modeladmin, request, queryset):
    queryset.update(verified=True)


make_verified_user.short_description = "Confirm user"


class MyUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('emil', 'password')}),
        (_('Personal info'), {'fields': (
        'first_name', 'last_name', 'email', 'chat_id', 'main_message_id', 'hash', 'auth_date', 'phone',
        'profile_picture', 'usertype', 'photo_url', )}),
        (_('Permissions'), {'fields': ('is_active','is_deleted', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ("first_name", "last_name", 'username', 'password1', 'password2'),
        }),
    )
    # The forms to add and change user instances
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    list_display = ('email', 'first_name', 'last_name', 'is_staff',)
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    list_display_links = ('email',)
    search_fields = ('first_name', 'last_name', 'username', 'email')
    ordering = ('-date_joined',)
    filter_horizontal = ('groups', 'user_permissions',)
    actions = [make_verified_user]


admin.site.register(User, MyUserAdmin)


# class UserPermissionAdmin(admin.ModelAdmin):
#     list_display = ['user','permission',]
# admin.site.register(UserPermission,UserPermissionAdmin)
admin.site.register(AllUserStatus)